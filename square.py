#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Based on MyPiShop code http://www.mypishop.com/Stepper.html
#
# Locate the drawing head in the centre of the Etch-a-sketch before running this.

import RPi.GPIO as GPIO
import time
import sys

pin1=17
pin2=18
pin3=21
#change next line to 27 if V2 Raspberry Pi
pin4=22
pin5=23
pin6=24
pin7=25
pin8=4

# set pin directions
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin1,GPIO.OUT)
GPIO.setup(pin2,GPIO.OUT)
GPIO.setup(pin3,GPIO.OUT)
GPIO.setup(pin4,GPIO.OUT)
GPIO.setup(pin5,GPIO.OUT)
GPIO.setup(pin6,GPIO.OUT)
GPIO.setup(pin7,GPIO.OUT)
GPIO.setup(pin8,GPIO.OUT)

Apin1=[0,1,0,0,1]        
Apin2=[0,1,1,0,0]
Apin3=[0,0,1,1,0]
Apin4=[0,0,0,1,1]

currenth=0
targeth=0
currentv=0
targetv=0

def HORIZONTAL(target,current):
	if current<target:
		while current<target:
			i=current&2+1
			GPIO.output(pin1,Apin1[i])
			GPIO.output(pin2,Apin2[i])
			GPIO.output(pin3,Apin3[i])
			GPIO.output(pin4,Apin4[i])
			time.sleep(.003)
			current=current+1
	else:
		while current>target:
			i=current&2 + 1
			GPIO.output(pin1,Apin1[i])
			GPIO.output(pin2,Apin2[i])
			GPIO.output(pin3,Apin3[i])
			GPIO.output(pin4,Apin4[i])
			time.sleep(.003)
			current=current-1
	print current,target
	return current;

def VERTICAL(target,current):
        if current<target:
                while current<target:
                        i=current&2+1
                        GPIO.output(pin5,Apin1[i])
                        GPIO.output(pin6,Apin2[i])
                        GPIO.output(pin7,Apin3[i])
                        GPIO.output(pin8,Apin4[i])
                        time.sleep(.003)
                        current=current+1
        else:
                while current>target:
                        i=current&2 + 1
                        GPIO.output(pin5,Apin1[i])
                        GPIO.output(pin6,Apin2[i])
                        GPIO.output(pin7,Apin3[i])
                        GPIO.output(pin8,Apin4[i])
                        time.sleep(.003)
                        current=current-1
        print current,target
        return current;

currenth=HORIZONTAL(10000,currenth)
currentv=VERTICAL(10000,currentv)
currenth=HORIZONTAL(0,currenth)
currentv=VERTICAL(0,currentv)
